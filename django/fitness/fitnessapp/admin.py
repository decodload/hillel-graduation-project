from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from fitnessapp.models import CustomUser, FoodCategory, FoodGroup, FoodInfo, Offer, UserFitStatistics, MealStage, Award, \
    AwardEvent
from django.utils.translation import gettext_lazy as _
from django.db.models import QuerySet


# Register your models here.


class UserProfileAdmin(UserAdmin):
    fieldsets = (
        (None, {"fields": ("username", "password")}),
        (
            _("Personal info"),
            {
                "fields": (
                    "first_name",
                    "last_name",
                    "profile_photo",
                    "email",
                    "is_email",
                    "phone",
                    "is_phone",
                    "weight",
                    "height",
                    "gender",
                    "is_gender",
                )
            }
        ),
        (
            _("Permissions"),
            {
                "fields": (
                    "is_active",
                    "is_staff",
                    "is_superuser",
                    "groups",
                    "user_permissions",
                ),
            },
        ),
        (
            _("Important dates"),
            {
                "fields": (
                    "last_login",
                    "date_joined"
                )
            }
        ),
        (
            _("Other"),
            {
                "fields": (
                    "date_of_birth",
                    "is_date_of_birth",
                    "country",
                    "is_country",
                    "city",
                    "is_city",
                    "is_statistics_all",
                    "is_statistics_week",
                    "is_statistics_month",
                )
            }
        ),
        (
            _("Total consumed kcal"),
            {
                "fields": (
                    "total_kcal_per_day",
                    "total_kcal_per_week",
                    "total_kcal_per_month",
                    "total_kcal_per_year",
                )
            }
        ),
    )


class FoodInfoAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            _("Food info"),
            {
                "fields": (
                    "food_name",
                    "portion",
                    "carbohydrates",
                    "proteins",
                    "fats",
                    "kilocalories",
                    "food_group",
                    "food_category",
                )
            }
        ),
    )

    list_display = ("food_name", "portion", "carbohydrates", "proteins", "fats", "kilocalories", "food_category")
    list_filter = ("food_category", "food_group",)
    search_fields = ("food_name", "carbohydrates", "proteins", "fats", "kilocalories")


class OfferAdmin(admin.ModelAdmin):

    fieldsets = (
        (
            _("Food info"),
            {
                "fields": (
                    "author",
                    "food_name",
                    "portion",
                    "carbohydrates",
                    "proteins",
                    "fats",
                    "kilocalories",
                    "food_group",
                    "food_category",
                )
            }
        ),
        (
            _("Other"),
            {
                "fields": (
                    "comment",
                    "offer_date",
                )
            }
        ),
    )

    list_display = ("food_name", "offer_status", "author", "comment")
    list_editable = ("offer_status",)
    list_filter = ("food_category", "food_group", "offer_status",)
    search_fields = ("food_name",)
    actions = ['apply_offer']

    @admin.action(description='Add to "Food Info" list and delete correct offer(s)')
    def apply_offer(self, request, queryset: QuerySet):
        for element in queryset.values():
            food_name = element['food_name']
            matching_names = FoodInfo.objects.values('food_name').filter(food_name__icontains=food_name)
            if len(matching_names) == 0:
                FoodInfo.objects.create(
                    food_name=element['food_name'],
                    portion=element['portion'],
                    carbohydrates=element['carbohydrates'],
                    proteins=element['proteins'],
                    fats=element['fats'],
                    kilocalories=element['kilocalories'],
                    food_category_id=element['food_category_id'],
                    food_group_id=element['food_group_id']
                )
                Offer.objects.filter(id=element['id']).delete()
            else:
                queryset.filter(id=element['id']).update(offer_status=Offer.offer_status_DECLINED)
        self.message_user(
            request,
            'All selected items completed successfully'
        )


class UserFitStatisticsAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            _("Change a note"),
            {
                "fields": (
                    "user",
                    "user_fit_statistics_date",
                    "meal_stage",
                    "real_portion",
                    "current_carbs",
                    "current_prot",
                    "current_fats",
                    "current_kcal",
                )
            }
        ),
    )

    list_display = ("user", "user_fit_statistics_date", "meal_stage", "real_portion", "current_kcal",)
    list_filter = ("user",)


class AwardAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            _("Change award"),
            {
                "fields": (
                    "award_name",
                    "award_picture",
                )
            }
        ),
        (
            _("For auto achievements per week"),
            {
                "fields": (
                    "least_kcal_per_week",
                    "most_kcal_per_week",
                )
            }
        ),
        (
            _("For auto achievements per month"),
            {
                "fields": (
                    "least_kcal_per_month",
                    "most_kcal_per_month",
                )
            }
        ),
        (
            _("For auto achievements per year"),
            {
                "fields": (
                    "least_kcal_per_year",
                    "most_kcal_per_year",
                )
            }
        ),
    )

    list_display = ("award_name",)


class AwardEventAdmin(admin.ModelAdmin):
    fieldsets = (
        (
            _("Change award"),
            {
                "fields": (
                    "award_date",
                    "award_name",
                    "remark",
                    "awarded_user",
                )
            }
        ),
    )

    list_display = ("award_name", "awarded_user", "award_date",)


admin.site.register(FoodCategory)
admin.site.register(FoodGroup)
admin.site.register(MealStage)
admin.site.register(UserFitStatistics, UserFitStatisticsAdmin)
admin.site.register(Award, AwardAdmin)
admin.site.register(AwardEvent, AwardEventAdmin)
admin.site.register(CustomUser, UserProfileAdmin)
admin.site.register(FoodInfo, FoodInfoAdmin)
admin.site.register(Offer, OfferAdmin)

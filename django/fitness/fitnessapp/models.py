from django.contrib.auth import get_user_model
from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import gettext_lazy as _


# Create your models here.

def profile_photo_upload_to(obj, file_name):
    return f'profile_photo/usr_{obj.id}/{file_name}'


def award_picture_upload_to(obj, file_name):
    return f'award_picture/award_{obj.id}/{file_name}'


class CustomUser(AbstractUser):
    is_email = models.BooleanField(_('Enable to show email in profile'), default=False)
    phone = models.CharField(max_length=11, blank=True, null=True)
    is_phone = models.BooleanField(_('Enable to show phone in profile'), default=False)
    weight = models.CharField(_('Your weight'), max_length=6, blank=True, null=True)
    height = models.CharField(_('Your height'), max_length=6, blank=True, null=True)
    gender = models.ForeignKey('fitnessapp.GenderIdentity', null=True, on_delete=models.CASCADE)
    is_gender = models.BooleanField(_('Enable to show gender in profile'), default=False)
    date_of_birth = models.DateField(_('Date of birth'), blank=True, null=True)
    age = models.IntegerField(blank=True, null=True)
    is_date_of_birth = models.BooleanField(_('Enable to show date of birth in profile'), default=False)
    country = models.CharField(max_length=255, blank=True, null=True)
    is_country = models.BooleanField(_('Enable to show country in profile'), default=False)
    city = models.CharField(max_length=255, blank=True, null=True)
    is_city = models.BooleanField(_('Enable to show city in profile'), default=False)
    profile_photo = models.ImageField(_('Profile photo'), blank=True, null=True, upload_to=profile_photo_upload_to)
    is_statistics_all = models.BooleanField(_('Enable to show statistics of your progress'), default=True)
    is_statistics_week = models.BooleanField(_('Enable to show statistics of your progress for the week'), default=True)
    is_statistics_month = models.BooleanField(_('Enable to show statistics of your progress for the month'), default=True)
    total_kcal_per_day = models.FloatField(_('Total kcal per day'), blank=True, null=True)
    total_kcal_per_week = models.FloatField(_('Total kcal per week'), blank=True, null=True)
    total_kcal_per_month = models.FloatField(_('Total kcal per month'), blank=True, null=True)
    total_kcal_per_year = models.FloatField(_('Total kcal per year'), blank=True, null=True)

    def __str__(self):
        return self.username


class GenderIdentity(models.Model):
    gender_id = models.CharField(_('Gender'), max_length=255, blank=True, null=True)

    def __str__(self):
        return self.gender_id


class FoodCategory(models.Model):
    food_category = models.CharField(max_length=255)

    class Meta:
        verbose_name_plural = _('Food categories')

    def __str__(self):
        return self.food_category


class FoodGroup(models.Model):
    food_group = models.CharField(max_length=255)

    def __str__(self):
        return self.food_group


class FoodInfo(models.Model):
    food_name = models.CharField(max_length=255)
    portion = models.IntegerField(_('Portion, g'))
    carbohydrates = models.FloatField(_('C(У)'))
    proteins = models.FloatField(_('P(П)'))
    fats = models.FloatField(_('F(Ж)'))
    kilocalories = models.IntegerField(_('kcal'))
    food_category = models.ForeignKey('fitnessapp.FoodCategory', null=True, on_delete=models.CASCADE)
    food_group = models.ForeignKey('fitnessapp.FoodGroup', null=True, on_delete=models.CASCADE)

    class Meta:
        verbose_name_plural = _('Food info')

    def __str__(self):
        return self.food_name


class Offer(models.Model):
    offer_status_NEW = 'new'
    offer_status_DECLINED = 'matched'
    offer_status_VERIFICATION = 'verification'

    OFFER_STATUS_CHOICES = [
        (offer_status_NEW, 'New offer'),
        (offer_status_DECLINED, 'Is matched'),
        (offer_status_VERIFICATION, 'Need revision')
    ]

    food_name = models.CharField(max_length=255)
    portion = models.IntegerField(_('Portion, g'))
    carbohydrates = models.FloatField(_('C(У)'))
    proteins = models.FloatField(_('P(П)'))
    fats = models.FloatField(_('F(Ж)'))
    kilocalories = models.IntegerField(_('kcal'))
    food_category = models.ForeignKey('fitnessapp.FoodCategory', null=True, on_delete=models.CASCADE)
    food_group = models.ForeignKey('fitnessapp.FoodGroup', null=True, on_delete=models.CASCADE)
    comment = models.TextField(_('Comment'), blank=True, null=True)
    author = models.ForeignKey(get_user_model(), null=True, on_delete=models.CASCADE)
    offer_date = models.DateTimeField(blank=True, null=True)
    offer_status = models.CharField(_('Status'), max_length=12, choices=OFFER_STATUS_CHOICES, default=offer_status_NEW)

    def __str__(self):
        return self.food_name


class MealStage(models.Model):
    meal_stage = models.CharField(_('Meal stage'), max_length=255, blank=True, null=True)

    def __str__(self):
        return self.meal_stage


class UserFitStatistics(models.Model):
    # чья запись
    user = models.ForeignKey(get_user_model(), null=True, on_delete=models.CASCADE)
    user_fit_statistics_date = models.DateField(_('Select date'), null=True)
    # завтрак - перекус - обед - перекус - ужин - ночной дожор
    meal_stage = models.ForeignKey('fitnessapp.MealStage', null=True, on_delete=models.CASCADE)
    food_info = models.ForeignKey('fitnessapp.FoodInfo', null=True, on_delete=models.CASCADE)
    real_portion = models.IntegerField(_('Your portion'))
    current_carbs = models.FloatField(_('C(У)'), blank=True, null=True)
    current_prot = models.FloatField(_('P(П)'), blank=True, null=True)
    current_fats = models.FloatField(_('F(Ж)'), blank=True, null=True)
    current_kcal = models.FloatField(_('Current kcal'), blank=True, null=True)

    class Meta:
        verbose_name_plural = _('User fit statistics')

    def __str__(self):
        return self.user.username


class Award(models.Model):
    award_name = models.CharField(_('Name of the award'), max_length=255, blank=True, null=True)
    award_picture = models.ImageField(_('Picture of the award'), blank=True, null=True, upload_to='awards')
    least_kcal_per_week = models.BooleanField(_('Consumed the least kcal per week'), default=False)
    most_kcal_per_week = models.BooleanField(_('Consumed the most kcal per week'), default=False)
    least_kcal_per_month = models.BooleanField(_('Consumed the least kcal per month'), default=False)
    most_kcal_per_month = models.BooleanField(_('Consumed the most kcal per month'), default=False)
    least_kcal_per_year = models.BooleanField(_('Consumed the least kcal per year'), default=False)
    most_kcal_per_year = models.BooleanField(_('Consumed the most kcal per year'), default=False)

    def __str__(self):
        return self.award_name


class AwardEvent(models.Model):
    award_date = models.DateField(blank=True, null=True)
    award_name = models.ForeignKey('fitnessapp.Award', null=True, on_delete=models.CASCADE)
    awarded_user = models.ForeignKey('fitnessapp.CustomUser', blank=True, null=True, on_delete=models.CASCADE)
    remark = models.CharField(max_length=255, blank=True, null=True)

    def __str__(self):
        return self.award_name.award_name

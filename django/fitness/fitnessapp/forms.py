import datetime

from django import forms
from django.contrib.auth import get_user_model
from django.db.models import Sum
from django.shortcuts import get_object_or_404
from django.utils import timezone
from fitnessapp.models import Offer, UserFitStatistics, FoodInfo


class RegistrationForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    check_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = get_user_model()
        fields = (
            'first_name',
            'last_name',
            'username',
            'email',
        )

    def clean_check_password(self):
        if self.cleaned_data['password'] != self.cleaned_data['check_password']:
            raise forms.ValidationError('Pass don\'t match')
        return self.cleaned_data['check_password']

    def save(self, commit=True):
        user = super(RegistrationForm, self).save(commit=False)
        user.set_password(self.cleaned_data['password'])
        user.save()
        return user


class EditProfileForm(forms.ModelForm):
    class Meta:
        model = get_user_model()

        exclude = (
            'password',
            'last_login',
            'is_active',
            'is_superuser',
            'is_staff',
            'is_superuser',
            'groups',
            'user_permissions',
            'date_joined',
            'age',
            'award',
            'total_kcal_per_day',
            'total_kcal_per_week',
            'total_kcal_per_month',
            'total_kcal_per_year',
        )

    def save(self, *args, **kwargs):
        user_profile = super(EditProfileForm, self).save(commit=False)
        if user_profile.date_of_birth:
            user_profile.age = (datetime.date.today() - user_profile.date_of_birth).days // 365
        user_profile.save()
        return user_profile


class OfferCreateForm(forms.ModelForm):

    class Meta:
        model = Offer
        exclude = (
            "author",
            "offer_date",
            "offer_status",
        )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(OfferCreateForm, self).__init__(*args, **kwargs)

    def save(self, *args, **kwargs):
        offer = super(OfferCreateForm, self).save(commit=False)
        offer.offer_date = timezone.now()
        offer.author = self.user
        offer.save()
        return offer


class UserFitStatisticsCreateForm(forms.ModelForm):

    class Meta:
        model = UserFitStatistics
        exclude = (
            "user",
            "current_carbs",
            "current_prot",
            "current_fats",
            "current_kcal"
        )

    def __init__(self, *args, **kwargs):
        self.user = kwargs.pop('user')
        super(UserFitStatisticsCreateForm, self).__init__(*args, **kwargs)

    def clean_real_portion(self):
        real_portion = self.cleaned_data['real_portion']
        if real_portion <= 0:
            raise forms.ValidationError('This field cannot be null and less or equal "0"')
        return real_portion

    def recalculate_total_kcal(self, **kwargs):
        if 'days' in kwargs:
            result = round(
                kwargs['notes'].filter(
                    user_fit_statistics_date__gte=datetime.datetime.today() - datetime.timedelta(days=kwargs['days'])
                ).aggregate(Sum('current_kcal'))['current_kcal__sum'], 2)
        else:
            result = round(kwargs['notes'].filter().aggregate(Sum('current_kcal'))['current_kcal__sum'], 2)
        return result

    def save(self, *args, **kwargs):
        user_fit_statistics = super(UserFitStatisticsCreateForm, self).save(commit=False)
        portion_norma = FoodInfo.objects.get(id=user_fit_statistics.food_info_id)
        user_fit_statistics.current_carbs = round(
            (user_fit_statistics.real_portion * portion_norma.carbohydrates / portion_norma.portion), 2)
        user_fit_statistics.current_prot = round(
            (user_fit_statistics.real_portion * portion_norma.proteins / portion_norma.portion), 2)
        user_fit_statistics.current_fats = round(
            (user_fit_statistics.real_portion * portion_norma.fats / portion_norma.portion), 2)
        user_fit_statistics.current_kcal = round(
            (user_fit_statistics.real_portion * portion_norma.kilocalories / portion_norma.portion), 2)
        user_fit_statistics.user = self.user
        user_fit_statistics.save()

        # перерасчет употребленных ккал пользователем за периоды: прошедший день, неделю, месяц и год
        user_profile = get_object_or_404(get_user_model(), username=self.user)
        notes = UserFitStatistics.objects.filter(
            user=self.user,
            user_fit_statistics_date__gte=datetime.datetime.today() - datetime.timedelta(days=365)
        ).order_by(
            'user_fit_statistics_date',
        )
        user_profile.total_kcal_per_day = self.recalculate_total_kcal(notes=notes, days=1)
        user_profile.total_kcal_per_week = self.recalculate_total_kcal(notes=notes, days=7)
        user_profile.total_kcal_per_month = self.recalculate_total_kcal(notes=notes, days=30)
        user_profile.total_kcal_per_year = self.recalculate_total_kcal(notes=notes)
        user_profile.save()
        return user_fit_statistics

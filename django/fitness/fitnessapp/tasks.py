import datetime

from fitness.celery import app
from django.contrib.auth import get_user_model

from datetime import datetime, timedelta


def award_event_create(award_name_id, awarded_user_id, remark=None):
    from fitness.models import AwardEvent

    AwardEvent.objects.create(
        award_date=datetime.date.today(),
        award_name_id=award_name_id,
        awarded_user_id=awarded_user_id,
        remark=remark
    )


@app.task
def award_event_weekly():
    from fitness.models import Award

    award_name = Award.objects.all()
    for element in award_name:
        if element.least_kcal_per_week:
            awarded_user = get_user_model().objects.all().values_list('id', flat=True).filter(
                total_kcal_per_week__gte=0
            ).order_by('total_kcal_per_week')[:1]
            award_event_create(award_name_id=element.id, awarded_user_id=awarded_user[0])

        if element.most_kcal_per_week:
            awarded_user = get_user_model().objects.all().values_list('id', flat=True).filter(
                total_kcal_per_week__gte=0
            ).order_by('-total_kcal_per_week')[:1]
            award_event_create(award_name_id=element.id, awarded_user_id=awarded_user[0])


@app.task
def award_event_monthly():
    from fitness.models import Award

    award_name = Award.objects.all()
    remark_date = datetime.date.today()
    for element in award_name:
        if element.least_kcal_per_month:
            awarded_user = get_user_model().objects.all().values_list('id', flat=True).filter(
                total_kcal_per_month__gte=0
            ).order_by('total_kcal_per_month')[:1]
            award_event_create(
                award_name_id=element.id,
                awarded_user_id=awarded_user[0],
                remark=(remark_date.replace(day=1) - datetime.timedelta(days=1)).strftime('%B %Y')
            )
        if element.most_kcal_per_month:
            awarded_user = get_user_model().objects.all().values_list('id', flat=True).filter(
                total_kcal_per_month__gte=0
            ).order_by('-total_kcal_per_month')[:1]
            award_event_create(
                award_name_id=element.id,
                awarded_user_id=awarded_user[0],
                remark=(remark_date.replace(day=1) - datetime.timedelta(days=1)).strftime('%B %Y')
            )


@app.task
def award_event_yearly():
    from fitness.models import Award
    award_name = Award.objects.all()
    for element in award_name:
        if element.least_kcal_per_year:
            awarded_user = get_user_model().objects.all().values_list('id', flat=True).filter(
                total_kcal_per_month__gte=0
            ).order_by('total_kcal_per_year')[:1]
            award_event_create(
                award_name_id=element.id,
                awarded_user_id=awarded_user[0],
                remark=datetime.date.today().year - 1
            )
        if element.most_kcal_per_year:
            awarded_user = get_user_model().objects.all().values_list('id', flat=True).filter(
                total_kcal_per_month__gte=0
            ).order_by('-total_kcal_per_year')[:1]
            award_event_create(
                award_name_id=element.id,
                awarded_user_id=awarded_user[0],
                remark=datetime.date.today().year - 1
            )

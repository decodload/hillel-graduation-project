"""fitness URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path, reverse_lazy
from . import views
from django.contrib.auth.views import LoginView, LogoutView

urlpatterns = [
    path('', views.IndexView.as_view(), name='home'),
    path('login/', LoginView.as_view(
        template_name="fitnessapp/login.html",
        next_page='/',
        extra_context={'title': 'Login'}
    ), name='login'),
    path('logout/', LogoutView.as_view(next_page=reverse_lazy('home')), name='logout'),
    path('registration/', views.RegistrationView.as_view(), name='registration'),
    path('profile/<int:id>/', views.ProfileView.as_view(), name='profile'),
    path('edit-profile/', views.EditProfileView.as_view(), name='edit-profile'),
    path('create-offer/', views.OfferCreateView.as_view(), name='create-offer'),
    path('add-note/', views.UserFitStatisticsCreateView.as_view(), name='add-note'),
    path('nutrition-table/', views.UserFitStatisticsView.as_view(), name='nutrition-table'),
    path('edit-note/<int:id>/', views.UserFitStatisticsUpdateView.as_view(), name='edit-note'),
    path('search-user/', views.SearchUser.as_view(), name='search-user'),
    path('nutrition-program/', views.NutritionProgram.as_view(), name='nutrition-program'),

] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + \
    static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

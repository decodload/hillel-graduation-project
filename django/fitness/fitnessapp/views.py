import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.shortcuts import get_object_or_404, redirect
from django.urls import reverse_lazy
from django.views.generic import TemplateView, FormView, DetailView, UpdateView, CreateView
from fitnessapp.forms import RegistrationForm, EditProfileForm, OfferCreateForm, UserFitStatisticsCreateForm
from fitnessapp.models import Offer, UserFitStatistics, AwardEvent
from django.contrib.auth import get_user_model


class IndexView(TemplateView):
    template_name = 'fitnessapp/index.html'

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        context.update({
            'title': 'Home Page',
        })
        return context


class RegistrationView(FormView):
    template_name = 'fitnessapp/registration.html'
    form_class = RegistrationForm
    success_url = reverse_lazy('login')

    def form_valid(self, form):
        form.save()
        return super(RegistrationView, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(RegistrationView, self).get_context_data(**kwargs)
        context.update({
            'title': 'Registration'
        })
        return context


class ProfileView(DetailView):
    model = get_user_model()
    template_name = 'fitnessapp/profile.html'
    pk_url_kwarg = 'id'

    def dispatch(self, request, *args, **kwargs):
        self.object = get_object_or_404(get_user_model(), id=self.kwargs['id'])
        return super(ProfileView, self).dispatch(request, *args, **kwargs)

    def get_user_awards(self, **kwargs):
        awards = AwardEvent.objects.filter(awarded_user=kwargs['id']).values(
            'award_name__award_name',
            'award_date',
            'awarded_user__username',
            'award_name__award_picture',
            'remark'
        )
        return awards

    def get_statistics(self, **kwargs):
        context = super(ProfileView, self).get_context_data(**kwargs)
        date_list = []
        meal_stage_list = []
        meal_stage_dict = {}
        total_composition_dict = {}
        if (not self.request.POST) or ('request_days' in self.request.POST):
            if not self.request.POST:
                request_days = 1
                period = f'Current {request_days} day'
            else:
                request_days = int(self.request.POST['request_days'])
                if request_days >= 7:
                    period = 'Last week'
                else:
                    period = f'Current {request_days} day'
            notes = UserFitStatistics.objects.select_related('meal_stage').filter(
                user=kwargs['id'],
                user_fit_statistics_date__gte=datetime.datetime.today() - datetime.timedelta(days=request_days)
            ).order_by(
                'user_fit_statistics_date',
                'meal_stage'
            )
        if 'request_month' in self.request.POST:
            period = 'Last month'
            datetime_now = datetime.date.today()
            if datetime_now.month >= 2:
                reporting_year = datetime_now.year
                reporting_month = datetime_now.month - 1
            else:
                reporting_year = datetime_now.year - 1
                reporting_month = 12
            notes = UserFitStatistics.objects.select_related('meal_stage').filter(
                user=kwargs['id'],
                user_fit_statistics_date__year=reporting_year,
                user_fit_statistics_date__month=reporting_month
            ).order_by(
                'user_fit_statistics_date',
                'meal_stage'
            )
        if 'start' and 'end' in self.request.POST:
            date_start = self.request.POST['start']
            date_end = self.request.POST['end']
            if date_start == '' and date_end != '':
                date_start = date_end
            elif date_start != '' and date_end == '':
                date_end = date_start
            elif date_start == '' and date_end == '':
                date_start = date_end = datetime.date.today()
            period = f'From {date_start} to {date_end}'
            notes = UserFitStatistics.objects.select_related('meal_stage').filter(
                user=kwargs['id'],
                user_fit_statistics_date__range=[date_start, date_end]
            ).order_by(
                'user_fit_statistics_date',
                'meal_stage'
            )

        if notes:
            for element in notes:
                if element.user_fit_statistics_date not in date_list:
                    date_list.append(element.user_fit_statistics_date)
                if element.meal_stage not in meal_stage_list:
                    meal_stage_list.append(element.meal_stage)
            amount_days = len(date_list)
            # разделение по приемам пищи
            for meal_stage in meal_stage_list:
                meal_stage_composition_dict = {}
                filtered_notes = notes.filter(meal_stage=meal_stage).aggregate(
                    Sum('current_carbs'),
                    Sum('current_prot'),
                    Sum('current_fats'),
                    Sum('current_kcal')
                )
                meal_stage_composition_dict.update({
                    f'current_carbs_avg_{meal_stage.meal_stage.lower().replace(" ", "_")}': round(
                        filtered_notes['current_carbs__sum'] / amount_days, 2),
                    f'current_prot_avg_{meal_stage.meal_stage.lower().replace(" ", "_")}': round(
                        filtered_notes['current_prot__sum'] / amount_days, 2),
                    f'current_fats_avg_{meal_stage.meal_stage.lower().replace(" ", "_")}': round(
                        filtered_notes['current_fats__sum'] / amount_days, 2),
                    f'current_kcal_avg_{meal_stage.meal_stage.lower().replace(" ", "_")}': round(
                        filtered_notes['current_kcal__sum'] / amount_days, 2)
                })
                meal_stage_dict.update({
                    f'{meal_stage.meal_stage.lower().replace(" ", "_")}': meal_stage_composition_dict
                })
            # пересчитать средние значения за весь период по приемам пищи и тотал
            total_notes = notes.filter().aggregate(
                Sum('current_carbs'),
                Sum('current_prot'),
                Sum('current_fats'),
                Sum('current_kcal')
            )
            total_composition_dict.update({
                'current_carbs_avg': round(total_notes['current_carbs__sum'] / amount_days, 2),
                'current_prot_avg': round(total_notes['current_prot__sum'] / amount_days, 2),
                'current_fats_avg': round(total_notes['current_fats__sum'] / amount_days, 2),
                'current_kcal_avg': round(total_notes['current_kcal__sum'] / amount_days, 2),
            })
        awards = self.get_user_awards(id=kwargs['id'])
        context.update({
            'awards': awards,
            'period': period,
            'meal_stage_dict': meal_stage_dict,
            'total_composition_dict': total_composition_dict,
            'title': f'Profile: {self.object.username}',
        })
        return context

    def get_context_data(self, **kwargs):
        context = self.get_statistics(id=self.object.id)
        return context

    def post(self, request, *args, **kwargs):
        context = self.get_statistics(id=kwargs['id'])
        return self.render_to_response(context=context)


class EditProfileView(LoginRequiredMixin, UpdateView):
    template_name = 'fitnessapp/edit-profile.html'
    model = get_user_model()
    form_class = EditProfileForm
    success_url = reverse_lazy('edit-profile')
    login_url = reverse_lazy('login')

    #  передаем объект (себя) в форму, минуя требуемый pk_url_kwarg = 'id' для UpdateView
    def get_object(self):
        edit_profile = get_user_model().objects.get(pk=self.request.user.id)
        return edit_profile

    def get_context_data(self, **kwargs):
        context = super(EditProfileView, self).get_context_data(**kwargs)
        context.update({
            'title': 'Edit profile'
        })
        return context


class OfferCreateView(LoginRequiredMixin, CreateView):
    template_name = 'fitnessapp/create-offer.html'
    model = Offer
    form_class = OfferCreateForm
    success_url = reverse_lazy('home')
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        form_kwargs = super(OfferCreateView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(OfferCreateView, self).get_context_data(**kwargs)
        context.update({
            'title': 'Create an offer'
        })
        return context


class UserFitStatisticsCreateView(LoginRequiredMixin, CreateView):
    template_name = 'fitnessapp/add-note.html'
    model = UserFitStatistics
    form_class = UserFitStatisticsCreateForm
    success_url = reverse_lazy('add-note')
    login_url = reverse_lazy('login')

    def get_form_kwargs(self):
        form_kwargs = super(UserFitStatisticsCreateView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(UserFitStatisticsCreateView, self).get_context_data(**kwargs)
        context.update({
            'title': 'Add a note'
        })
        return context


class UserFitStatisticsView(LoginRequiredMixin, TemplateView):
    template_name = 'fitnessapp/nutrition-table.html'
    model = UserFitStatistics
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        date_list = []
        kcal_list = []
        context = super(UserFitStatisticsView, self).get_context_data(**kwargs)
        notes = UserFitStatistics.objects.select_related(
            'meal_stage',
            'food_info__food_group',
            'food_info__food_category'
        ).filter(user=self.request.user.id).order_by(
            '-user_fit_statistics_date',
            'meal_stage'
        )
        for element in notes:
            if element.user_fit_statistics_date not in date_list:
                date_list.append(element.user_fit_statistics_date)
        for element in date_list:
            sum_kcal = notes.filter(user_fit_statistics_date=element).aggregate(Sum('current_kcal'))
            sum_kcal.update({
                'current_kcal__sum': round(sum_kcal['current_kcal__sum'], 2),
                'user_fit_statistics_date': element
            })
            kcal_list.append(sum_kcal)
        context.update({
            'notes': notes,
            'kcal_list': kcal_list,
            'title': 'Nutrition table'
        })
        return context


class UserFitStatisticsUpdateView(LoginRequiredMixin, UpdateView):
    template_name = 'fitnessapp/add-note.html'
    model = UserFitStatistics
    form_class = UserFitStatisticsCreateForm
    success_url = reverse_lazy('nutrition-table')
    pk_url_kwarg = 'id'
    login_url = reverse_lazy('login')

    # проверка, является ли редактор хозяином записи
    def dispatch(self, request, *args, **kwargs):
        note = UserFitStatistics.objects.get(id=self.kwargs['id'])
        if note.user_id != self.request.user.id:
            return redirect('nutrition-table')
        return super(UserFitStatisticsUpdateView, self).dispatch(request, *args, **kwargs)

    def get_form_kwargs(self):
        form_kwargs = super(UserFitStatisticsUpdateView, self).get_form_kwargs()
        form_kwargs['user'] = self.request.user
        return form_kwargs

    def get_context_data(self, **kwargs):
        context = super(UserFitStatisticsUpdateView, self).get_context_data(**kwargs)
        context.update({
            'title': 'Edit note'
        })
        return context


class SearchUser(LoginRequiredMixin, TemplateView):
    model = get_user_model()
    template_name = 'fitnessapp/search-user.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super(SearchUser, self).get_context_data(**kwargs)
        if 'search_user' in self.request.GET:
            users = get_user_model().objects.all().filter(username__contains=self.request.GET['search_user'])
        else:
            users = get_user_model().objects.all()
        context.update({
            'users': users,
            'title': 'Search user',
        })
        return context


class NutritionProgram(LoginRequiredMixin, TemplateView):
    template_name = 'fitnessapp/nutrition-program.html'
    login_url = reverse_lazy('login')

    def get_context_data(self, **kwargs):
        context = super(NutritionProgram, self).get_context_data(**kwargs)
        context.update({
            'title': 'Nutrition program',
        })
        return context

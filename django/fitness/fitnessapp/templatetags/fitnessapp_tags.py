
from django import template
from django.contrib.auth import get_user_model
from fitnessapp.models import AwardEvent

register = template.Library()


@register.filter()
def amount_is_correct(text, amount):
    amount = float(amount)
    current_amount = float(text)
    # отклонение на 10% (согласно справочнику "Стеля")
    deviation = amount * 20 / 100
    if current_amount <= 0.0:
        text = f'<span style="color:red" title="Don\'t skip meals, it\'s bad for your health =/">' \
               f'{current_amount}</span>'
        return text
    elif (amount - deviation) <= current_amount <= (amount + deviation):
        text = f'<span style="color:black">{current_amount}</span>'
    elif current_amount < (amount - deviation):
        text = f'<span style="color:grey" title="Amount is below normal. You need to balance your food">' \
               f'{current_amount}</span>'
    elif current_amount > (amount + deviation):
        text = f'<span style="color:orange" title="Amount is higher than normal. You need to balance your food">' \
               f'{current_amount}</span>'
    return text


@register.simple_tag()
def least_kcal():
    top_10 = get_user_model().objects.all().filter(total_kcal_per_day__gte=0).order_by('total_kcal_per_day')[:10]
    return top_10


@register.simple_tag()
def most_kcal():
    top_10 = get_user_model().objects.all().filter(total_kcal_per_day__gte=0).order_by('-total_kcal_per_day')[:10]
    return top_10


@register.simple_tag()
def awarded_users_list():
    user_list = AwardEvent.objects.all().values(
        'award_name__award_name',
        'award_date',
        'awarded_user',
        'awarded_user__username',
        'award_name__award_picture',
        'remark'
    ).order_by('-award_date')[:100]
    return user_list
